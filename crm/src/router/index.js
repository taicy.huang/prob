import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Vuex from "vuex";

Vue.use(Vuex);
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    meta: { requiresAuth: false },
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Login.vue"),
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    children: [
      {
        path: "/product",
        name: "AddProduct",
        meta: { requiresAuth: true },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/products/List.vue"),
      },
      {
        path: "/productUpd",
        name: "productUpd",
        meta: { requiresAuth: true },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/products/Edit.vue"),
      },
      {
        path: "/productList",
        name: "productList",
        meta: { requiresAuth: true },
        component: () =>
          import(
            /* webpackChunkName: "about" */ "../views/products/AllList.vue"
          ),
      },
    ],
  },
  {
    path: "*",
    redirect: "/login",
  },
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isLogin = localStorage.getItem("token") == "ImLogin";
  
  let needLogin = to.matched.some(record => {
    return record.meta.requiresAuth;
  })

  if (needLogin) {
    if (isLogin) {
      next();
    } else {
      next({ path: '/' });
    }
  } else {
    next(); // 往下繼續執行
  }
});
export default router;
