import axios from "axios";
import productList from "../resource/productList.json";
import productDetail from "../resource/productDetail.json";

// import qs from "qs";

axios.defaults.baseURL = "http://172.105.205.37:888/api/";
// axios.defaults.baseURL = "http://127.0.0.1:8000/api/";
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    if (err && err.response) {
      switch (err.response.status) {
        case 403:
          router.replace({
            name: "Login",
          });
          break;
        case 404:
          console.log("找不到該頁面");
          break;
        case 405:
          console.log("method 用錯");
          break;
        case 444:
          console.log("444 token過期");

          router.replace({
            name: "Login",
          });

          break;
        case 500:
          console.log("伺服器出錯");
          break;
        case 503:
          console.log("服務失效");
          break;
        default:
          console.log(`連接錯誤${err.response.status}`);
      }
    } else {
      console.log("連接到服務器失敗");
    }
    return Promise.resolve(err.response);
  }
);

export const apiLogin = (data) => axios.post("login", data);
export const apiProductsList = () => axios.get("product");
export const apiProductsDetail = (data) => axios.post("product/detail", data);
export const apiProductDelete = (params) => axios.delete("product", params);
export const apiDeleteImg = (data) => axios.post("delImg", data);
export const apiDeleteAll = (data) => axios.post("delAll", data);
export const apiEdit = (data) => axios.post("edit", data);
export const apiUploadImg = (data) => axios.post("uploadImg", data);

export const getProductList = () => productList;
export const getProductDetail = (params) => productDetail;
